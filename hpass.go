/* Copyright (C) 2015,2020 Cristoffer Kvist

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

package main

import (
	"bufio"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"golang.org/x/crypto/ssh/terminal"
	"os"
	"os/signal"
	"strings"
	"time"
)

var pwdLen int = 20            // Number of chars of encrypted string to print
var timeOut time.Duration = 30 // Timeout in seconds

func main() {

	// Catch CTRL-C interrupt and handle them in the select loop
	c := make(chan os.Signal, 0)
	signal.Notify(c, os.Interrupt)

	fmt.Print("Password: ")
	pb, _ := terminal.ReadPassword(int(os.Stdin.Fd()))
	pass := string(pb)

	fmt.Println("\rEnter string to encrypt. CTRL-D to exit.\n")

	input := make(chan string)

	// Scan for input string
	go func(input chan string) {
		// Send a LF on startup to get the prompt
		input <- string(10) // send: LF

		for {
			scanner := bufio.NewScanner(os.Stdin)
			e := scanner.Scan()
			str := scanner.Text()

			if e == false {
				// received CTRL+D
				input <- string(24) // send: CAN
			} else if len(str) == 0 {
				// received New Line
				input <- string(10) // send: LF
			} else {
				input <- str
			}
		}
	}(input)

	// Process input string - encrypt or quit (by user or timeout)
	var output string
	for {
		mac := hmac.New(sha256.New, []byte(pass))

		select {
		case <-c: // CTRL-C (+ other) signals
			clearCmdLine("\n", output)
			return
		case str := <-input:
			if str[0] == 10 { // LF
				clearCmdLine("", output)
				fmt.Printf("> ")
			} else if str[0] == 24 { // CAN - Cancel, quit
				clearCmdLine("\n", output)
				return
			} else {
				clearCmdLine("", str)

				mac.Write([]byte(str))
				output = encodeToString(mac.Sum(nil), pwdLen)
				fmt.Printf(output)
			}
		case <-time.After(timeOut * time.Second):
			clearCmdLine("\n", output)
			return
		}
	}
}

func clearCmdLine(pri, chrLen string) {
	blankStr := strings.Repeat(" ", len(chrLen)+2)
	fmt.Printf(pri + "\x1B[1F" + blankStr + "\r")
}

func encodeToString(m []byte, pLen int) string {
	return base64.StdEncoding.EncodeToString(m)[:pLen]
}

Hpass
=====
Hpass is a go (golang) command line application for encrypting a string with a password using sha256 and hmac.

Dependencies
============
Hpass depends on the terminal package found here: [https://godoc.org/golang.org/x/crypto/ssh/terminal](https://godoc.org/golang.org/x/crypto/ssh/terminal)

Status
======

License
=======
Released under MIT license.
